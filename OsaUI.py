#
# This module allows you open OSA UI and imitate user actions
#

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

# this class initialize selenium objects and logins to OSA
class OSAui:
    def __init__(self, url, username, password):
        self.url = url
        self.username = username
        self.password = password

    def wdstart(self):
        """Start display and driver to have possibility to load OSA control panel"""

        display = Display(visible=0, size=(1024, 768))
        display.start()
        self.driver = webdriver.Firefox()

    def osalogin(self):
        """login to OSA cp"""

        driver = self.driver
        driver.get('https://cp.edu.trn')

        # wait when frame is loaded
        element = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.NAME, "loginFrame")))

        # switch to login frame
        driver.switch_to_frame("loginFrame")

        # making sure that element is present
        element = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.ID, "inp_user")))

        # locate user and input data
        elmt_user = driver.find_element_by_id("inp_user")
        elmt_user.send_keys("admin")

        # input password and send the form
        elmt_pwd = driver.find_element_by_id("inp_password")
        elmt_pwd.send_keys("setup")
        driver.find_element_by_name("login").click()

        # wait when frame is loaded
        element = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.NAME, "topFrame")))

    def wdclose(self):
        self.driver.close()

# This class inherits from OSAui and has one more method: switch to billing
# provider control panel and choose "offers" menu item to vefiry that panel is
# fully functioning
class BillingSwitch(OSAui):

    def switch_to_bm(self):
        """switch from Operation to Billing and choose orders tab. If all these
           actions were correct - the panel is working.
        """
        driver = self.driver
        # switch to billing to check that BA is also working
        driver.switch_to_frame("topFrame")

        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "to_bm")))

        elmt_bm = driver.find_element_by_id("to_bm")
        elmt_bm.click()

        driver.switch_to_window(driver.window_handles[0])

        # wait when frame is loaded
        element = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.NAME, "leftFrame")))

    #Check that orders item is working
    def billing_availability(self):

        driver = self.driver

        driver.switch_to.frame("leftFrame")
        orders = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "#click_orders > span > b")))

# This class uploads encrypted key to billing
class UploadKey(BillingSwitch):

    def load_key(self, encrypt_key, key_pwd):

        driver = self.driver
        driver.switch_to.frame("leftFrame")


        settings = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.ID, "click_newsystem_settings")))

        #Go to billing settings
        elmt_settings = driver.find_element_by_css_selector("#click_newsystem_settings > span > b")

        elmt_settings.location_once_scrolled_into_view #to move elmt into visible area of browser

        elmt_settings.click()

        driver.switch_to_window(driver.window_handles[0])

        #Go to encryption keys
        driver.switch_to_frame("mainFrame")
        enc_keys_elmt = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.ID, "sys_set_operations_e_commerce_keys_management")))

        elmt_enc_key = driver.find_element_by_id("sys_set_operations_e_commerce_keys_management")
        elmt_enc_key.click()

        #Go to private key load page
        load_prvt_key = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.ID, "input___loadPK1")))

        load_key_elmt = driver.find_element_by_id("input___loadPK1")
        load_key_elmt.click()

        #Click on load private key from local disk
        load_to_srv = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.ID, "input___LoadToServer")))

        load_to_elmt = driver.find_element_by_id("input___LoadToServer")
        load_to_elmt.click()

        #Fill form with data
        vendor_key = WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.ID, "input___SP_ViewVendorKey")))

        #Specify key file
        choose_file_elmt = driver.find_element_by_id("input___FileWithKey")
        choose_file_elmt.send_keys(encrypt_key)

        #Specify password
        choose_file_elmt = driver.find_element_by_id("input___SpwdComm")
        choose_file_elmt.send_keys(key_pwd)

        #Click to load key
        vendor_key_elmt = driver.find_element_by_id("input___SP_ViewVendorKey")
        vendor_key_elmt.click()



