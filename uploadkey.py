#!/usr/local/bin/python2.7

from OsaUI import UploadKey

#initialize connection to UI
myosa = UploadKey('https://cp.edu.trn','user','password')
myosa.wdstart()

#login to billing provider control panel
myosa.osalogin()
myosa.switch_to_bm()

#Load key
myosa.load_key("/data/PrivateKey_1.bmk","1Q2W3E1q2w3e")

#close display
myosa.wdclose()