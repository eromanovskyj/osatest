#!/usr/local/bin/python2.7

from OsaUI import BillingSwitch
import unittest

class TestBilling(unittest.TestCase):
    def test_billing_availability(self):

        #initialize connection to UI
        self.myosa = BillingSwitch('https://cp.edu.trn','user','password')
        self.myosa.wdstart()
        self.myosa.osalogin()
        self.myosa.switch_to_bm()
        self.myosa.billing_availability()

    def tearDown(self):
        self.myosa.wdclose()

if __name__ == "__main__":
        unittest.main()